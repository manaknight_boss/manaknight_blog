---
title: >-
  TOP 3 Reasons Your Company Should Choose Custom Solution Over Off The Shelf Solution (Non-techy people)
tags:
  - Software
  - Custom
  - Company
  - Off-the-shelf
categories:
  - Business
cover: /img/business-meeting.jpg
subtitle: Most of the time it doesn't fix your business problem exactly
author: Ryan
description: We are always on the look out for any solution to solve our business problems no matter how large or small. Because let’s be honest here, we all have problems. Software is one type of tool to solve these problems.
date: 2018-04-15 15:01:47
---

We are always on the look out for any solution to solve our business problems no matter how large or small. Because let's be honest here, we all have problems. Software is one type of tool to solve these problems.

## Definitions

### What is custom/bespoke software?
Custom/Bespoke software is software that an software agency makes from scratch from the required features you provide to them. This software can be a website or a mobile application or a desktop app you install on your computer. If it's a website, they can host it on a cloud server(server not in your office) or host it in a computer in your office. If it's a mobile application, they will host the mobile app on the apple app store and google play store. If it's a desktop app, they will give you an file which you can double click and run the program.

### What is off the shelf solution?
Off the shelf solution is software that is premade with a fixed set of features. You then pay a subscription/pay to use/yearly license to use their software. Software can take to form of a website, mobile application or desktop app. They usually ask you to login or provide some sort of license key to use the software. They don't usually take request for custom features and what you see is what you get.

## Reasons

Below I'll list the top 3 reasons to go custom software over off the shelf solution.

### Reason 1: It Just Doesn’t Fix Your Business Problem/Process
Many times they have many features and integration with many other software. However, they don't have your particle feature you desperately need. 

Common examples:
- not exporting to excel and only allow csv
- not integrate to your accounting software you use
- software is too hard to figure out and use to something simple for you
- not work on your computer operating system (Window/MacOS/Linux)
- not accepting custom feature request

So you pay for their software and you still don't solve your problem. When you do the custom software, a consultant(like me) will sit down with you and understand your business process with you and create features you want and need that fits your business exactly.

### Reason 2: Price Comparison
Many times people will decide to go off the shelf solution just off of price. However, most pricing is very deceptive to calculate the cost. When your an enterprise company, price is not as important compare to SMB company. I'll give a few examples below:

**Monthly Plan**
So a lot of off the shelf solutions use a monthly subscription model. Let's say
$99 per month. So that's $1188 per year. 2 years it's $2376. 3 years it's $3564. 4 years it's $4752. 5 years $5940. 6 years $7128. 

| Off the shelf solution        | Custom Solution           |
| ------------- |:-------------:|
| $99 per month<br/>$1188 per year<br/> $2376 in 2 years<br/>$3564 in 3 years<br/> $4752 in 4 years<br/> $5940 in 5 years <br/> $7128 in 6 years     | $4000 and up One Time |


So if you are expecting to run your business for next 5+ years(who wouldn't) then the pricing is relatively the same in 4 years and beyond. Why should you have to struggle with a software that barely fixes your problem when you can make something that works for you day 1 at same cost?

### Reason 3: Ownership
When you use off the shelf solution, they don't let you move your information off their platform. There's 2 reasons for this. 
- they have their own data model, so can't remove your data easily
- they want you to keep paying them to use their platform

When you have a custom solution, you own the data on your own server either in house or on the cloud.

You also don't own the infrastructure when you use off the shelf solution. They may use your data for their own purpose like selling data, making their own analytic models. By not owning the infrastructure, you could be losing your valuable intellectual property data or client list. This won't happen in a custom solution as you own the server.

You also cannot restrict access in off the shelve solution. Let say you only want your team to access this software in your office only. You can't do this on off the shelve solution. On custom software you can put any amount of restriction on who and how can someone access your software.

There are many more reason but these are the most important to focus on. If you ever want help on solving your business problem's, you can book a chat with manaknightdigital on the link  below. 

https://manaknightdigital.com/schedule.html
