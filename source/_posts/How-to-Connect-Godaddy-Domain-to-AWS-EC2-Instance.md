---
title: How to Connect Godaddy Domain to AWS EC2 Instance
tags:
  - Hosting
  - Server
  - Devops
categories:
  - Hosting
cover: /img/server.png
subtitle: Step by Step guide
date: 2018-05-09 11:15:53
---

There isn't a very clear step by step guide to connect godaddy to AWS server so here's a full guide.

# Godaddy Section
1. Login to Godaddy and go to this page https://account.godaddy.com/products/
2. Click Manage button on your domain
3. Scroll to the bottom and click "Manage DNS"
4. Go to Nameservers and make sure they are "ns75.domaincontrol.com" and "ns76.domaincontrol.com"
5. Assuming you've made your AWS server(You can do this later), click Add button under Records and add Type A Record with Host being @ and Points to IP address of AWS Server and TTL 1 hour. Click Save.
6. Click Add button under Records and add Type CNAME Record with Host being www and Points to @ and TTL 1 hour. Click Save.

# AWS Section
1. Login to AWS https://us-west-2.console.aws.amazon.com/console (You maybe on different region)
2. Click Services on navigation and go to EC2
3. Click launch Instance, select your machine (Prefer Ubuntu 16.04)
4. Click Next: Configure Instance Details Button
5. Configure the rest to your situation.
6. Click on your server row and click Connect. There's instructions for you to login to the server with SSH
7. Click into Network & Security Group -> Security Group
8. Find your security group for your server.
9. Click Action -> Edit inbound rules
10. Make sure you have a rule for Type:HTTP, Protocol TCP, Port 80, Source 0.0.0.0/0
11. Make sure you have a rule for Type:HTTP, Protocol TCP, Port 80, Source ::/0
12. Make sure you have a rule for Type:SSH, Protocol TCP, Port 22, Source 0.0.0.0/0
13. Make sure you have a rule for Type:HTTP, Protocol TCP, Port 443, Source 0.0.0.0/0
14. Make sure you have a rule for Type:HTTP, Protocol TCP, Port 443, Source ::/0

These steps should be enough to help you setup a working server.

If you want a fast way to setup a PHP Server with nginix, mysql, ftp and everything else you need, follow steps below.

# Setup PHP Web Server
1. Go to https://easyengine.io/
2. Run this "wget -qO ee rt.cx/ee && sudo bash ee" in AWS SSH.
3. source /etc/bash_completion.d/ee_auto.rc
4. sudo ee stack install
5. ee site create example.com --php --mysql --letsencrypt
6. sudo service nginx restart
7. sudo crontab -e
8. Add this line "sudo ee site update example.com --letsencrypt=renew >> ~/renew.log"
9. sudo service crontab reload
10. sudo service crontab restart

# Add FTP User
1. sudo passwd www-data
2. grep www-data /etc/passwd
3. sudo nano /etc/passwd, change /usr/sbin/nologin to /bin/bash
4. sudo nano /etc/ssh/sshd_config, change PasswordAuthentication No to Yes
5. sudo service ssh restart

# Migrating Site
If your migrating a site from shared hosting to AWS, there maybe some DNS caching issue where you keep seeing the old site but everywhere else says its correct. To verify this, use a VPN to check your site. I've had this issue where site been cached for days. If your using wordpress, I would suggest you use duplicator plugin as it will save you a lot of time.

# Deleted lets encrypt keys by accident
If you deleted the keys in /etc/letsencrypt/live/domain, here's how you resolve it.
Where domain is your domain

```
cd /etc/letsencrypt/live/domain
rm cert.pem chain.pem fullchain.pem privkey.pem
ln -s ../../archive/domain/cert2.pem cert.pem
ln -s ../../archive/domain/chain2.pem chain.pem
ln -s ../../archive/domain/fullchain2.pem fullchain.pem
ln -s ../../archive/domain/privkey2.pem privkey.pem
```

If you ever want help on solving your business problem's, you can book a chat with manaknightdigital on the link below. 

https://manaknightdigital.com/schedule.html
