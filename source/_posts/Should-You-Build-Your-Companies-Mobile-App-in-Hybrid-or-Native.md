---
title: Should You Build Your Company's Mobile App in Hybrid or Native App Technology?
tags:
  - Hybrid App
  - Mobile
categories:
  - Mobile
cover: /img/mobile-app.jpg
subtitle: Indepth look by a company that does both Hybrid and Native Apps
date: 2018-07-21 13:15:28
---

There's a lot of articles on this topic but they are usually very bias to one direction since people writing these articles don't do both. As a company, we have done both types of projects and can offer more indepth analysis into doing both.

# What is Native App Development?
Mobile applications are programs people install on their phone. The most popular phones in the market are Android and iOS. As a result, developers need to make their companies mobile application twice, once in Android and once in iOS. Since both phone types don't use the same programming language you need to code the same app twice. Native App Development gives you fine grain control of all aspects of your device. They also allow you to make crazy widgets on the phone. Both phone types have a unique user experience from each other. For example, Android prefer the navigation up top, while iOS prefer the navigation at the bottom. Users in each ecosystem can tell when the app doesn't feel natural to their ecosystem.

## What are PROS and CONS of using Native App Development?

| PROS        |            CONS           |
| ------------- |:-------------:|
|You get fine grain control of everything on the phone. So if you have performance issues, you can dig in and solve it | You will developers with expertises in both Android and iOS |
| Android and iOS have different phone sizes. Image assets need to be sized for particular resolution size. You have the flexibility to add images for different phone sizes.| Your designer will need to do more work making image assets for different resolutions.|
|You always get access to the latest and greatest features Android and iOS has to offer.| If Android or iOS updates their codebase, they may depreciate some old code. You may need to update your app to support new features in newer devices.|
|Any feature Android & iOS doesn't support can be implemented yourself by coding low level libraries ||

# What is Hybrid App Development?
Hybrid App Development is leveraging web development tools and skill to make mobile applications. In web, you use HTML, CSS and Javascript to make your websites. To make use of these web tools, Hybrid Development frameworks usually create a skeleton barebone Android and iOS app that opens a webview (similar to your browser but for mobile). Then in this webview, your web code for the app is run inside so it feels like a real app. Usually hybrid apps have the same look and feel on both phone types which people can notice. To use native device features, there's an open source technology called cordova which helps developers use native app features in webview.




## What are PROS and CONS of using Hybrid App Development?

| PROS        |            CONS           |
| ------------- |:-------------:|
|You can write the app once and deploy it on both Android & iOS | Your limited features by the webview. If you need more performance, you can't do much since your at the mercy of the webview. For example videos aren't played the same way as the native app would. |
| Hybrid Frameworks usually have styled components so you can make pages relatively fast | Android and iOS have different phone sizes. Image assets won't be scaled by resolution so images maybe distorted on larger phones |
|If Android or iOS updates their code version, it doesn't really affect your app. You can support lower version of android or iOS without worrying if it will break your app.| Your limited to what libraries Cordova has available to you. If the library is bad, your stuck. If library is buggy your stuck. 


# What are Native Apps built using Javascript?
There's another flavor of developing apps where you can code the app in javascript and they make native apps. These frameworks are React Native, NativeScript, Flutter and a few others. Essentially, you are building your app using javascript like a website but the framework takes your code and converts it into native code to run in the native app. There a slight delay in performance compare to native apps but less noticable than Hybrid Apps. The downside of these technology is the same as hybrid app development. Your depending on the publisher of these frameworks to make tools and libraries for you to use to make device specific features. If they didn't make that library you need, your stuck.

# So how do you decide what to use?
At the end of the day, if you have they money, always go native. Going Hybrid or javascript should only be used as a cost cutting measure or basic prototype. 

The Mobile App landscape has changed since the early days. People download and install new apps daily and the experience really matters. If the user can't figure out how to use the app on the first try, they are going uninstall the app(I do this often as well). 

Is it worth it to have a reduced performance app and risk losing new customers? 

Is it worth losing a customer because the customer doesn't feel this app fits into their phone ecosystem(Using the same design layout on both phones)?

In my opinion, you should use hybrid technology only as a prototype to showcase what your product does.

If your app functions exactly like your mobile website, then build your Native app built using javascript. You won't need much special device features and users can still have a full mobile app experience.

Under all other circumstances, you should go native to give your company the best chance of getting adopted.

There are many more reason but these are the most important to focus on. If you ever want help on this topic, you can book a chat with manaknightdigital on the link below. 

https://manaknightdigital.com/schedule.html
