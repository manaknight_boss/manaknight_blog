---
title: TOP 3 Reasons You Should Use VPS over Shared Hosting (For Non-Techy People)
tags:
  - Hosting
  - Server
categories:
  - Hosting
cover: /img/server.png
subtitle: Cheap hosting doesn't mean good hosting
date: 2018-04-22 19:49:43
description: Have you ever seen a server hosting for less than $5 per month? Learn why you will be paying more than $5 per month once you get past the checkout page. The first question is, how do I know which web host is Shared Hosting or not. In general, any web hosting plans that starts at $5/month. This means that they give you some space to host your website on a server and that is it.
author: Ryan
---

I am sure everyone has seen cheap hosting ads for less than $5 per month. However, have you ever looked at the fine print or know what they are actually offering? Most people are not aware of how bad they are. I will give a break down for what they provide. 

### Shared Hosting
The first question is, well Ryan how do I know which web host is Shared Hosting. In general, any web hosting plans that starts at $5/month. This means that they give you some space to host your website on a server and that is it. 

There is NO GUARANTEE of memory or computing power. For example, if you suddenly get more traffic to your website. Your website will be slowed down to a halt and there is nothing you can do. Since you share this server with other sites, if another site spikes in traffic it will slow your site down. 

You DO NOT HAVE ACCESS to low level server capabilities. You are limited to what the hosting provider allows. Most hosting providers only provide you with the ability to host PHP code. If you want a simple Wordpress site, this works fine. However, if you need to install other programming languages or advanced frameworks it is forbidden. 

If other sites hosted on your server gets their IP blacklisted, you will as well. Your customer’s web browser will show a page before loading your website stating your website is too dangerous to enter. This will severely damage your company’s reputation. One of the biggest downsides is getting DDOS(Denial of Service) Attacked. This happens when millions of servers hit your site consistently causing you to run out of computing resource and forcing your site to not respond to anyone. An attack on other sites hosted will affect you as well.

### VPS (Virtual Private Server)
Now we get to discuss VPS. These plans usually go for $5 and up. This is the ideal plan for developers and software businesses. What happens here is a server is broken into tiny pieces and sold to individual site owners. Each piece has Dedicated Memory and Computing Power. You have full control of the server. You can install any software you like. You are not affected by other’s blacklisted IP or DDOS attack on others. You get full performance of your server share. 

### Dedicated Server
This plan cost the most. Depending on the provider, the cost can be $100+/month. This plan lets you be the sole user on a server. For demanding software that needs every drop of performance, this is your plan.

## Reasons

### Reason 1: Resources
I would never recommend anyone to use the shared hosting plan as it gives you nothing. It is fine for a simple company website with a contact form but that is it. If you expect more than 10 visitors a day, you need something with more power. In my opinion, the best plan for most businesses is the VPS plan, which gives the best performance for its price. You can do anything on it that you desire. Dedicated Servers are ideal for high intense traffic site that needs more than average computing/memory usage. Good examples of this are Ad servers, Massively Multiplayer Online Games and Real time software. 

### Reason 2: Cost
The cost is roughly the same, may as well go VPS.

When using shared hosting, they usually come with a program called cPanel and other management software. When something goes wrong, there is nothing you can do besides what is available in the panel. VPS plans allow you to control the server’s command line. 

You can optimize the server for performance, block bad traffic from hitting your server, seal vulnerable ports from hackers and implement preventive measures against DDOS attacks.

There are many more reasons on Shared Hosting versus VPS but these are the most important to focus on. If you ever want help on solving your business problems, you can book a chat with manaknightdigital on the link below.

| Shared Hosting        | VPS           |
| ------------- |-------------|
|Bluehost ($2.75/month)| digitalocean($5+/month)|
|1and1($0.99/month)|AWS(Free first year)|
|siteground($3.95/month)|Vultr($2.5+/month)|
|mediatemple($20/month)|Azure (Free first year)|
|hostgator($3.95/month)||


### Reason 3: Control
When using shared hosting, they usually come with cPanel and other management software. When something goes wrong theres nothing you can do besides whats available in the panel. VPS plans allow you to control the server's commandline. You can optimize the server for performance, you can block bad traffic from hitting your server, you can seal ports hackers come in from. You can put in preventive measures for DDOS attacks.

There are many more reason but these are the most important to focus on. If you ever want help on solving your business problem's, you can book a chat with manaknightdigital on the link  below. 

https://manaknightdigital.com/schedule.html

