# Steps

1.Create new post

```
hexo new post "title"
```

2.Write the content in markdown
- https://en.support.wordpress.com/markdown-quick-reference/[https://en.support.wordpress.com/markdown-quick-reference/]

3.Start Server to see content

```
hexo server

//generate the html files
hexo generate
```

4.Commit change
```
git commit -a -m "Site Updated Month Day Hour:Minute"
git push origin master
```

5.Deploy

```
git checkout deploy
git merge master
:q
hexo generate
git commit -a -m "Site Updated Month Day Hour:Minute"
git push origin deploy
```